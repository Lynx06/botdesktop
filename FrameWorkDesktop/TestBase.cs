﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using WindowsInput;
using WindowsInput.Native;

namespace PDV
{
    public abstract class TestBase
    {
        #region Globals

        public readonly string FileName = @"C:\Windows\System32\calc.exe";
        public readonly string Proc = "calc.exe";
        public DesiredCapabilities Dc;
        private TestContext _testContext;
        public Actions Action;
        public ExtentReports Extent;
        public static ExtentTest Test;
        public static Screenshot Screenshot;
        public ExtentHtmlReporter HtmlReporter;
        public static string Logger { get; set; }
        public static RemoteWebDriver Driver { get; set; }
        public static InputSimulator Simulator;

        #endregion

        [TestInitialize]
        public void MyTestInitialize()
        {
            ExecuteCmd($"taskkill /im {Proc} /f /t");
            ExecuteCmd("taskkill /im Winium.exe /f /t");
            Process.Start(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Drivers", "Winium.exe"));
            Dc = new DesiredCapabilities();
            Dc.SetCapability("app", FileName);
            Driver = new RemoteWebDriver(new Uri("http://localhost:9999"), Dc);
            Action = new Actions(Driver);
            Simulator = new InputSimulator();
        }

        [TestCleanup]
        public void MyTestCleanup()
        {
            ExecuteCmd($"taskkill /im {Proc} /f /t");
            ExecuteCmd("taskkill /im Winium.exe /f /t");
        }

        public TestContext TestContext
        {
            get { return _testContext; }
            set { _testContext = value; }
        }

        #region Methods

        public static string ConvertImageToBase64(string fileName)
        {
            byte[] imageArray = File.ReadAllBytes(fileName);
            return Convert.ToBase64String(imageArray);
        }

        public string GetErrorMessage()
        {
            const BindingFlags privateGetterFlags = BindingFlags.GetField |
                                                    BindingFlags.GetProperty |
                                                    BindingFlags.NonPublic |
                                                    BindingFlags.Instance |
                                                    BindingFlags.FlattenHierarchy;

            var mMessage = string.Empty; // Returns empty if TestOutcome is not failed
            if (TestContext.CurrentTestOutcome == UnitTestOutcome.Failed)
            {
                // Get hold of TestContext.m_currentResult.m_errorInfo.m_stackTrace (contains the stack trace details from log)
                var field = TestContext.GetType().GetField("m_currentResult", privateGetterFlags);
                if (field != null)
                {
                    var mCurrentResult = field.GetValue(TestContext);
                    field = mCurrentResult.GetType().GetField("m_errorInfo", privateGetterFlags);
                    if (field != null)
                    {
                        var mErrorInfo = field.GetValue(mCurrentResult);
                        field = mErrorInfo.GetType().GetField("m_stackTrace", privateGetterFlags);
                        if (field != null) mMessage = field.GetValue(mErrorInfo) as string;
                    }
                }
            }

            return mMessage;
        }

        public List<KeyValuePair<string, string>> ExecuteCmd(string command)
        {
            Process process = new Process();
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();

            process.StartInfo.FileName = "cmd.exe";
            process.StartInfo.Arguments = "/C " + command;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.Start();
            process.WaitForExit();

            list.Add(new KeyValuePair<string, string>("Output", process.StandardOutput.ReadToEnd()));
            list.Add(new KeyValuePair<string, string>("Error", process.StandardError.ReadToEnd()));

            Thread.Sleep(3000);

            return list;
        }

        public static IWebElement GetElement(By by, int timeoutSeconds = 10)
        {
            WaitElement(by, timeoutSeconds);

            if (Driver.FindElements(by).Count > 0)
            {
                return Driver.FindElement(by);
            }

            return null;
        }

        public static bool WaitElement(By by, int timeoutSeconds = 10)
        {
            int count = 0;
            bool displayed = false;

            while (Driver.FindElements(by).Count.Equals(0) || !displayed)
            {
                if (Driver.FindElements(by).Any(e => e.Displayed))
                {
                    displayed = true;
                }

                Thread.Sleep(1000);

                count++;

                if (count > timeoutSeconds)
                {
                    break;
                }

            }

            return displayed;
        }

        #endregion

        #region Keyboard

        public static void KeysDown(VirtualKeyCode text, int time = 1, int repeat = 1)
        {
            for (int i = 0; i < repeat; i++)
            {
                try
                {
                    Thread.Sleep(time * 1000);

                    Simulator.Keyboard.KeyDown(text);

                    Thread.Sleep(time * 1000);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + " " + ex.InnerException);
                }
            }
        }

        #endregion
    }
}