﻿using OpenQA.Selenium;
using PDV;

namespace FrameWorkDesktop.PageObjects
{
    public class CalcPage : TestBase
    {
        #region Elements / Actions

        public static void ClickNumeroUmTeste()
        {
            var findId = Driver.FindElementById("131");

            findId.Click();
        }

        #endregion


        #region Elements

        public static By NumeroUm()
        {
            return By.Name("1");
        }

        public static By Adicao()
        {
            return By.Name("Adicionar");
        }

        #endregion

        #region Actions

        #region Clicks

        //public static void ClickNumeroUm(int timeoutSeconds = 10)
        //{
        //    Logger = "Click Botão Um";
        //    Driver.Click(NumeroUm(), timeoutSeconds);
        //}

        public static void ClickAdicao(int timeoutSeconds = 10)
        {
            Logger = "Click Botão Adição";
            Driver.Click(Adicao(), timeoutSeconds);
        }

        #endregion

        #endregion
    }
}