﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;

namespace PDV
{
    public static class Util
    {
        public static bool FailIfNotExist = true;

        public static void Click(this RemoteWebDriver driver, By by, int timeoutSeconds = 3)
        {
            bool action = false;

            for (int i = 0; i < timeoutSeconds; i++)
            {
                try
                {
                    driver.FindElement(by).Click();
                    action = true;
                }
                catch (Exception)
                {
                    // To Do
                }

                if (action)
                {
                    break;
                }
            }

            if (!action && FailIfNotExist)
            {
                driver.FindElement(by).Click();
            }
        }

        public static void SendKeys(this RemoteWebDriver driver, By by, string text, int timeoutSeconds = 5)
        {
            bool action = false;

            for (int i = 0; i < timeoutSeconds; i++)
            {
                try
                {
                    driver.FindElement(by).SendKeys(text);
                    action = true;
                }
                catch (Exception)
                {
                    // To Do
                }

                if (action)
                {
                    break;
                }
            }

            if (!action && FailIfNotExist)
            {
                driver.FindElement(by).SendKeys(text);
            }
        }

    }
}